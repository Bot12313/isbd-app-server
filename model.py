import datetime

from peewee import *

# test_big
database = PostgresqlDatabase('tk7', **{'user': 'postgres', 'password': 'admin'})


class BaseModel(Model):
	class Meta:
		database = database


class User(BaseModel):
	mail = CharField(index=True)
	password = TextField()
	phone = CharField(null=True, unique=True)
	twofa = CharField()

	class Meta:
		table_name = 'user'
		schema = 'public'


class Card(BaseModel):
	about = TextField(null=True)
	birthday = DateField(index=True)
	name = CharField()
	res1_id = IntegerField()
	res2_id = IntegerField()
	res3_id = IntegerField()
	search_age_from = IntegerField(index=True, null=True)
	search_age_to = IntegerField(index=True, null=True)
	search_sex = BooleanField(null=True)
	sex = BooleanField()
	user = ForeignKeyField(column_name='user_id', field='id', model=User, primary_key=True)

	class Meta:
		table_name = 'card'
		schema = 'public'


class Pair(BaseModel):
	card_a = ForeignKeyField(column_name='card_a_id', field='user_id', model=Card)
	card_b = ForeignKeyField(backref='user_card_b_set', column_name='card_b_id', field='user_id', model=Card, null=True)
	state = IntegerField()
	unique_key = TextField(index=True)

	class Meta:
		table_name = 'pair'
		schema = 'public'


class File(BaseModel):
	file_path = TextField(unique=True)
	type = IntegerField()
	user = ForeignKeyField(column_name='user_id', field='id', model=User)

	class Meta:
		table_name = 'file'
		schema = 'public'


class Message(BaseModel):
	message_type = IntegerField()
	pair = ForeignKeyField(column_name='pair_id', field='id', model=Pair)
	text = TextField(null=True)
	timestamp = DateTimeField(default=datetime.datetime.now)
	user = ForeignKeyField(column_name='user_id', field='id', model=User, null=True)
	uuid = TextField(unique=True)

	class Meta:
		table_name = 'message'
		schema = 'public'


class Attachment(BaseModel):
	file = ForeignKeyField(column_name='file_id', field='id', model=File)
	message = ForeignKeyField(column_name='message_id', field='id', model=Message)

	class Meta:
		table_name = 'attachment'
		indexes = (
			(('file', 'message'), True),
		)
		schema = 'public'
		primary_key = CompositeKey('file', 'message')


class CardFile(BaseModel):
	card = ForeignKeyField(column_name='card_id', field='user', model=Card)
	file = ForeignKeyField(column_name='file_id', field='id', model=File)
	file_order = IntegerField()

	class Meta:
		table_name = 'card_file'
		indexes = (
			(('card', 'file'), True),
		)
		schema = 'public'
		primary_key = CompositeKey('card', 'file')


class Restaurant(BaseModel):
	description = TextField()
	title = CharField()
	user = ForeignKeyField(column_name='user_id', field='id', model=User)

	class Meta:
		table_name = 'restaurant'
		schema = 'public'


class Coupon(BaseModel):
	code = CharField(unique=True)
	description = TextField()
	restaurant = ForeignKeyField(column_name='restaurant_id', field='id', model=Restaurant)
	title = CharField(null=True)
	usages_left = IntegerField(index=True, null=True)

	class Meta:
		table_name = 'coupon'
		schema = 'public'


class Location(BaseModel):
	coord_x = DoubleField()
	coord_y = DoubleField()
	timestamp = DateTimeField()
	user = ForeignKeyField(column_name='user_id', field='id', model=User)

	class Meta:
		table_name = 'location'
		schema = 'public'


class Report(BaseModel):
	reported_id = IntegerField()
	reported_text = TextField()
	reported_type = IntegerField()
	status = IntegerField()
	timestamp = DateTimeField()
	user = ForeignKeyField(column_name='user_id', field='id', model=User)

	class Meta:
		table_name = 'report'
		schema = 'public'


class RestaurantBranch(BaseModel):
	address = TextField()
	coord_x = DoubleField()
	coord_y = DoubleField()
	restaurant = ForeignKeyField(column_name='restaurant_id', field='id', model=Restaurant)
	title = TextField(null=True)

	class Meta:
		table_name = 'restaurant_branch'
		schema = 'public'


class Support(BaseModel):
	access_level = IntegerField(index=True)
	name = CharField()
	user = ForeignKeyField(column_name='user_id', field='id', model=User, primary_key=True)

	class Meta:
		table_name = 'support'
		schema = 'public'
