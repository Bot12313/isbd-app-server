import datetime
import random
from uuid import uuid4
from datetime import timedelta
from pprint import pprint
import string
import json
from flask_cors import CORS
from hashlib import sha256
import peewee
from flask import Flask, request, session, make_response
from flask_restful import Resource, Api
from playhouse.shortcuts import model_to_dict

from model import *

import ssl

context = ssl.SSLContext()
context.load_cert_chain('cert.pem', 'key.pem', password="1234")

app = Flask(__name__)
app.config.update(
	SESSION_COOKIE_SECURE=True,
	SESSION_COOKIE_HTTPONLY=True,
	SESSION_COOKIE_SAMESITE='None',
)

CORS(app, origin="http://localhost:3000", supports_credentials=True)

app.config['SECRET_KEY'] = b'_5#y2L"F4Q8z\n\xec]/'
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
api = Api(app)


@app.before_request
def before_request():
	database.connect()
	#
	session.permanent = True
	app.permanent_session_lifetime = timedelta(days=15000)


@app.after_request
def after_request(response):
	database.close()
	return response


class ResUser(Resource):
	def get(self, mail):
		response = "ok", 200
		if mail is None:
			response = "mail is missing", 400
		else:
			try:
				person = User.get(User.mail == mail)
				session["id"] = person.id
				response = person.id, 200
			except DoesNotExist:
				response = "mail not found", 404

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


class MyCard(Resource):
	def get(self):
		response = "ok", 200
		if not "id" in session:
			response = "wrong id", 400
		else:
			id = session.get("id")
			try:
				user = User.get(User.id == id)
				card = Card.get(Card.user == user)
				response = model_to_dict(card, backrefs=False), 200
			except DoesNotExist:
				response = "id not found", 404
		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object

	def put(self):
		response = "ok", 200
		data = json.loads(request.stream.read(int(request.environ.get('CONTENT_LENGTH'))))
		if not "id" in session:
			response = "wrong id", 400
		else:
			id = session.get("id")
			try:
				user = User.get(User.id == id)
				card = Card.get(Card.user == user)

				for key in data.keys():
					data[key] = None if data[key] == '' else \
						False if data[key] == "false" else \
							True if data[key] == "true" else data[key]
					setattr(card, key, data[key])

				print("rows modified:", card.save())
				response = "ok", 200
			except DoesNotExist as er:
				response = "id not found", 404
			except Exception as er:
				response = "something wrong", 500
				print(er)
		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


class ResCard(Resource):
	def get(self, id):
		response = "ok", 200
		try:
			user = User.get(User.id == id)
			card = Card.get(Card.user == user)
			response = model_to_dict(card, backrefs=False), 200
		except DoesNotExist:
			response = "id not found", 404
		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		print(response)
		return response_object


class ResPair(Resource):
	def get(self):
		response = "ok", 200
		if not "id" in session:
			response = "wrong id", 400
		else:
			id = session.get("id")
			try:
				user = User.get(User.id == id)
				pairs = Pair.select().where(
					((Pair.card_a == user) | (Pair.card_b == user)) & (Pair.state == 2))  # created | dialog
				resp_list = []
				for pair in pairs:
					resp_list.append(model_to_dict(pair, recurse=False, backrefs=False))
				response = resp_list, 200
			except DoesNotExist:
				response = "id not found", 404

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


def random_coupon(len=5):
	return sha256(''.join(random.choice(string.ascii_letters) for _ in range(len)).encode()).hexdigest().upper()[:5]


class PairMessages(Resource):
	def get(self, pair_id):
		response = "ok", 200
		try:
			pair = Pair.get(Pair.id == pair_id)
			messages = Message.select().where(Message.pair == pair)
			resp_list = []

			for message in messages:
				resp_list.append(model_to_dict(message, recurse=False, backrefs=False))
			response = resp_list, 200
		except DoesNotExist:
			response = "id not found", 404

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object

	def post(self, pair_id):
		data = json.loads(request.stream.read(int(request.environ.get('CONTENT_LENGTH'))))
		if "idemp_key" in data and "text" in data and len(data["text"]) > 0:
			if not "id" in session:
				response = "wrong id", 400
			else:
				id = session.get("id")
				try:
					pair = Pair.get(Pair.id == pair_id)
					user = User.get(User.id == id)
					message = Message(message_type=0, pair=pair, text=data["text"], user=user, uuid=data["idemp_key"])
					message.save()
					response = model_to_dict(message, recurse=False, backrefs=False), 200
				except DoesNotExist:
					response = "id not found", 404
				except:
					response = "something wrong, it could be the 'idemp_key'", 400
		else:
			response = "data should contain 'idemp_key' and 'text' keys", 400

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


class ResRestaurant(Resource):
	def get(self, id):
		response = "ok", 200
		try:
			rest = Restaurant.get(Restaurant.id == id)
			response = model_to_dict(rest, recurse=False, backrefs=False), 200
		except DoesNotExist:
			response = "id not found", 404

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


class Search(Resource):
	def get(self):
		response = "ok", 200
		if not "id" in session:
			response = "wrong id", 400
		else:
			id = session.get("id")
			try:
				user = User.get(User.id == id)
				card = Card.get(Card.user == user)
				pairs = Pair.select().where((Pair.card_b == user) & (Pair.state == 0))  # created | dialog
				if len(pairs) == 0:

					existing_pairs = [pair.unique_key for pair in Pair.select(Pair.unique_key)]
					where_clause = [
						Card.user_id != id,
						fn.CONCAT(Card.user_id, '_', id).not_in(existing_pairs),
						fn.CONCAT(id, '_', Card.user_id).not_in(existing_pairs),
					]

					search_age_from = card.search_age_from
					if search_age_from is not None:
						date_years_ago = datetime.datetime.now() - datetime.timedelta(days=search_age_from * 365)
						where_clause.append(Card.birthday <= date_years_ago)

					search_age_to = card.search_age_to
					if search_age_to is not None:
						date_years_ago = datetime.datetime.now() - datetime.timedelta(days=search_age_to * 365)
						where_clause.append(Card.birthday >= date_years_ago)

					search_sex = card.search_sex
					if search_sex is not None:
						where_clause.append(Card.sex == search_sex)

					search = Card.select().where(*where_clause).limit(1)
					print("THE FUCK:", search, ", ", where_clause)
					if len(search) > 0:
						found = search[0]
						print(card, found)
						unique_key = str(min(id, found.user_id)) + "_" + str(max(id, found.user_id))
						pair = Pair(card_a=card, card_b=found, state=0, unique_key=unique_key)
						pair.save()

						found = model_to_dict(search[0], backrefs=False)
						found["new"] = True
						print("Search found:", found, ", ukey:", unique_key, ", pair:", pair)
						response = [found], 200
					else:
						response = [], 200
				else:
					found = model_to_dict(pairs[0].card_a, backrefs=False)
					found["new"] = False
					response = [found], 200

			except DoesNotExist:
				response = "id not found", 404

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object

	def put(self, other_id):
		response = "ok", 200
		if not "id" in session:
			response = "wrong id", 400
		else:
			if other_id is not None:
				my_id = session.get("id")
				try:
					unique_key = str(min(my_id, other_id)) + "_" + str(max(my_id, other_id))
					pair = Pair.get(Pair.unique_key == unique_key)
					if pair.state == 0:
						if pair.card_a_id == my_id:
							response = "ok", 200
						else:
							restaurants = [
								pair.card_a.res1_id,
								pair.card_a.res2_id,
								pair.card_a.res3_id,
								pair.card_b.res1_id,
								pair.card_b.res2_id,
								pair.card_b.res3_id,
							]
							random_restaurant_id = random.choice(restaurants)
							random_restaurant = Restaurant.get(Restaurant.id == random_restaurant_id)
							coupon = random_coupon()

							pair.state = 2  # dialog
							pair.save()

							text = 'Вам был выдан купон {coupon} от {restaurant}!'.format(coupon=coupon,
							                                                              restaurant=random_restaurant.title)

							first_message = Message(
								pair=pair,
								message_type=1,
								text=text.format(),
								timestamp=datetime.datetime.now(),
								user=None,
								uuid=uuid4()
							)
							first_message.save()

							response = "ok", 200
					else:
						response = "wrong pair state", 400
				except DoesNotExist:
					response = "id not found", 404
			else:
				response = "other_id not found", 404

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object

	def delete(self, other_id):
		response = "ok", 200
		if not "id" in session:
			response = "wrong id", 400
		else:
			if other_id is not None:
				my_id = session.get("id")
				try:
					unique_key = str(min(my_id, other_id)) + "_" + str(max(my_id, other_id))
					pair = Pair.get(Pair.unique_key == unique_key)
					print("Search delete:", pair, ", ukey:", unique_key)
					if pair.state == 0:
						pair.state = 1  # refused
						pair.save()
						response = "ok", 200
					else:
						response = "wrong pair state", 400
				except DoesNotExist:
					response = "id not found", 404
			else:
				response = "other_id not found", 404

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


class Restaurants(Resource):
	def get(self):
		response = "ok", 200
		restaurants = Restaurant.select()
		resp_list = []
		for restaurant in restaurants:
			resp_list.append(model_to_dict(restaurant, recurse=False, backrefs=False))
		response = resp_list, 200

		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


class MyCoupons(Resource):
	def get(self):
		response = "ok", 200
		if not "id" in session:
			response = "wrong id", 400
		else:
			id = session.get("id")
			try:
				user = User.get(User.id == id)

				messages = Message.select(Message.text).distinct(Message.pair).join(Pair).where(
					(Message.message_type == 1) & ((Pair.card_a == user) | (Pair.card_b == user)))
				resp_list = []
				for message in messages:
					resp_list.append(message.text)
				response = resp_list, 200
			except DoesNotExist:
				response = "id not found", 404
		response_object = make_response(json.dumps({"data": response[0]}, indent=4, sort_keys=True, default=str),
		                                response[1])
		return response_object


api.add_resource(ResUser, '/user/<string:mail>')
api.add_resource(MyCard, '/card/my')
api.add_resource(ResCard, '/card/<int:id>')
api.add_resource(ResPair, '/pairs')
api.add_resource(PairMessages, '/pair/<int:pair_id>/messages')
api.add_resource(ResRestaurant, '/restaurant/<int:id>')

api.add_resource(Search, '/search', '/search/<int:other_id>')
api.add_resource(Restaurants, '/restaurants')
api.add_resource(MyCoupons, '/coupons/my')


@app.route('/')
def hello_world():
	return 'Hello, World!'


if __name__ == "__main__":
	# from waitress import serve
	app.run(host="25.45.63.230", port="8080", ssl_context=context)
# 25.45.63.230
